= Comprendre et mettre en place un serveur Radius sous Windows server
:lang: fr
:encoding: utf-8
:imagesdir: /infradoc/network/auth/media

== Qu’est ce qu’un serveur RADIUS?

RADIUS (Remote Authentication Dial-In User Service) est un protocole client-serveur permettant de centraliser des données d'authentification. Le protocole RADIUS a été inventé et développé en 1991 par la société Livingston enterprise (rachetée par Lucent Technologies), qui fabriquait des serveurs d'accès au réseau pour du matériel uniquement équipé d'interfaces série; il a fait ultérieurement l'objet d'une normalisation par l'IETF.

== Fonctionnement d’un RADIUS

image::schema_radius.png[Schéma Radius]

Le fonctionnement de RADIUS est basé sur un scénario proche de celui-ci (schéma ci-dessus):

* Un utilisateur envoie une requête au NAS afin d’autoriser une connexion à distance;
* Le NAS  achemine la demande au serveur RADIUS;
* Le serveur RADIUS consulte la base de données d’identification afin de connaître le type de scénario d’identification démandé pour l’utilisateur. Soit le scénario actuel convient, soit une autre méthode d’identification est demandée à l’utilisateur.

== Les étapes d’installation d’un serveur RADIUS (Windows serveur)

=== Installation du serveur RADIUS

* Dans le gestionnaire windows serveur vous allez ajouter un rôle (Add Roles and Features) et dans «Server Roles» selectionnez l’option «Network Policy and Access services» 
* Cliquez sur «Add Features»
* Ensuite allez sur «Role service» et cochez «Network Policy Server»
* Et confirmez et cliquez sur «Install»

=== Intégration Active Directory

Après l’installation du serveur RADIUS vous devez créer un groupe d'utilisateurs autorisés à s'authentifier à l'aide de Radius. Créez le groupe RADIUS-USER (par exemple)

=== Ajouter des périphériques clients

* Après avoir intégrer un groupe sur le serveur, ouvrez l’application «Network policy Server».Vous devez autoriser le serveur Radius sur la base de données Active Directory. Clique-droit sur NPS (local) et sélectionnez l’option «register server in Active Directory»;
* Ensuite il faut configurer les clients RADIUS. Les clients Radius sont des périphériques qui seront autorisés à demander l'authentification du serveur Radius. **Important! Ne confondez pas les clients Radius avec les utilisateurs Radius**. Clique-droit sur le dossier Radius Clients et sélectionnez l'option Nouveau.
* Vous devez définir la configuration suivante:
** Nom convivial du périphérique.
** Adresse IP du périphérique
** Secret partagé par périphérique.

Le secret partagé sera utilisé pour autoriser le périphérique à utiliser le serveur Radius.

=== Configurer une stratégie réseau

Maintenant, vous devez créer une police de réseau pour permettre l'authentification.

* Cliquez avec le bouton droit sur le dossier Stratégies de réseau et sélectionnez l'option Nouveau. Entrez un nom pour la stratégie réseau et cliquez sur le bouton Suivant.
* Cliquez sur le bouton Ajouter une condition. Nous allons permettre aux membres du groupe RADIUS-SERS de s'authentifier.
* Sélectionnez l'option Groupe d'utilisateurs et cliquez sur le bouton Ajouter.
* Cliquez sur le bouton Ajouter des groupes et localisez le groupe RADIUS-USERS.
* Sélectionnez l'option Accès accordé et cliquez sur le bouton Suivant. Cela permettra aux membres du groupe RADIUS-USERS de s'authentifier sur le serveur Radius.
* Je définis la méthode d’authentification Authentification non chiffrée (PAP, SPAP).